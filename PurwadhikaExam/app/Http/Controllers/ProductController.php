<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\UnitRumah;

class ProductController extends Controller
{
    function getUnit(){

        $data = DB::table('unit')->get();

        return response()->json($data, 200);
    }

    function createUnit(Request $request){
        DB::beginTransaction();

        try{
            //save to database
            $this->validate($request, [
                'no_rumah' => 'required',
                'harga_rumah' => 'required',
                'luas_tanah' => 'required',
                'luas_bangunan' => 'required'
            ]);

            //eloquent
            $usr = new UnitRumah;
            $usr->kavling = $request->input('kavling');
            $usr->blok = $request->input('blok');
            $usr->no_rumah = $request->input('no_rumah');
            $usr->harga_rumah = $request->input('harga_rumah');
            $usr->luas_tanah = $request->input('luas_tanah');
            $usr->luas_bangunan = $request->input('luas_bangunan');
            $usr->customer_id = $request->input('customer_id');
            $usr->save();

            $unitrumah = UnitRumah::get();

            DB::commit();
            return response()->json($unitrumah, 200);

        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }

    function deleteUnit(Request $request){

       DB::beginTransaction();
        try{

            $this->validate($request, [
                'id' => 'required',
            ]);

            DB::delete('delete from unit where id = ?', [$request->id]);

            DB::commit();
             return response()->json('berhasil', 200);
        }
        
        catch(\Exception $e){
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}
